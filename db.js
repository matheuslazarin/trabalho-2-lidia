const mongoClient = require("mongodb").MongoClient;
mongoClient
  .connect("mongodb://localhost/clients", { useNewUrlParser: true })
  .then(conn => (global.conn = conn.db("clients")))
  .catch(err => console.log(err));

function findAll(callback) {
  global.conn
    .collection("clients")
    .find({})
    .toArray(callback);
}

function insertOne(client, callback) {
  global.conn.collection("clients").insertOne(client, callback);
}

const ObjectId = require("mongodb").ObjectId;
function findOne(id, callback) {
  global.conn
    .collection("clients")
    .find(new ObjectId(id))
    .toArray(callback);
}

function update(id, client, callback) {
  global.conn
    .collection("clients")
    .updateOne({ _id: new ObjectId(id) }, { $set: client }, callback);
}

function deleteOne(id, callback) {
  global.conn
    .collection("clients")
    .deleteOne({ _id: new ObjectId(id) }, callback);
}

module.exports = { findAll, insertOne, findOne, update, deleteOne };
